from django.urls import path
from todos.views import todo_list, show_todo


urlpatterns = [
    path("", todo_list, name="todos"),
    path("<int:id>/", show_todo, name="show_todo"),
]
